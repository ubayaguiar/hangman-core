module.exports = function getErrorsFromHistory(word, history) {
  return history.reduce((prev, letter) => (
    word.includes(letter) ? prev : prev + 1
  ), 0)
}
